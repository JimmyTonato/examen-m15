package m15.pe1.newadnmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author tarda
 */
/*URL EJERCICIO GITLAB clic: https://gitlab.com/JimmyTonato/examen-m15*/
public class DnaNewExamMain {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        DnaNewExamMain myApp = new DnaNewExamMain();
        myApp.run();
    }

    /**
     * Function that runs the app
     */
    private void run() {
        int option = 0;
        ADN_Manager cadenaADN = new ADN_Manager();

        String dnaSequence = null;
        do {
            menuPrincipal();
            Scanner myScan = new Scanner(System.in);
            option = myScan.nextInt();
            switch (option) {
                case 0:
                    System.out.println("Bye!");
                    break;
                case 1:
                    ADNFileReader file = new ADNFileReader();
                    ArrayList<String> dnaSequence_list = file.readSequence("src/main/java/m15/pe1/newadnmanager/dnaSequence.txt");
                    dnaSequence = String.join("", dnaSequence_list);
                    dnaSequence = dnaSequence.toUpperCase();
                    option = 0;
                    break;
                case 2:
                    ADNWrite adnTeclado = new ADNWrite();
                    dnaSequence = adnTeclado.secuencia();
                    option = 0;
                    break;
                default:
                    System.out.println("La opció seleccionada no és válida.");
                    break;

            }
        } while (option != 0);

            do {
                menu();
                Scanner myScan = new Scanner(System.in);
                option = myScan.nextInt();
                myScan.nextLine();

                switch (option) {
                    case 0:
                        System.out.println("Bye!");
                        break;
                    case 1:
                        System.out.println("**Donar la volta " + dnaSequence + "**");
                        System.out.println("");
                        System.out.println(cadenaADN.invertADN(dnaSequence));
                        break;
                    case 2:
                        System.out.println("**Trobar la base més repetida " + dnaSequence + "**");
                        System.out.println("");
                        System.out.println(cadenaADN.maxLetter(dnaSequence));
                        break;
                    case 3:
                        System.out.println("**Trobar la base menys repetida " + dnaSequence + "**");
                        System.out.println("");
                        System.out.println(cadenaADN.minLetter(dnaSequence));
                        break;
                    case 4:
                        System.out.println("**Fer recompte de bases " + dnaSequence + "**");
                        System.out.println("");
                        System.out.println(TOTAL_DE + " A: " + cadenaADN.numAdenines(dnaSequence));
                        System.out.println(TOTAL_DE + " C: " + cadenaADN.numCitosines(dnaSequence));
                        System.out.println(TOTAL_DE + " G: " + cadenaADN.numGuanines(dnaSequence));
                        System.out.println(TOTAL_DE + " T: " + cadenaADN.numTimines(dnaSequence));
                        break;
                    case 5:
                        System.out.println("**Fer porcentaje de bases " + dnaSequence + "**");
                        System.out.println("");
                        System.out.println("Porcentaje de A: "+ cadenaADN.porcentajes(dnaSequence, 'A')+"%");
                        System.out.println("Porcentaje de C: "+ cadenaADN.porcentajes(dnaSequence, 'C')+"%");
                        System.out.println("Porcentaje de G: "+ cadenaADN.porcentajes(dnaSequence, 'G')+"%");
                        System.out.println("Porcentaje de T: "+ cadenaADN.porcentajes(dnaSequence, 'T')+"%");
                        break;
                    default:
                        System.out.println("La opció seleccionada no és válida.");
                        break;
                }
            } while (option != 0);

    }
/**
 * Funcion donde imprimimos el menu secuendario donde encuetras las opciones sobre la cadena
 */
    public static void menu() {
        System.out.println("\n Tria una opció:");
        System.out.println("0.-Sortir");
        System.out.println("1.-Donar la volta\n"
                + "2.- Trobar la base més repetida,\n"
                + "3.- Trobar la base menys repetida\n"
                + "4.- Fer recompte de bases\n"
                + "5.- Fer compte de bases(porcentajes)");
        System.out.print("Opció: ");
    }
/**
 * Funcion del menu donde eliges si desde el archivo o desde el teclado
 */
    public static void menuPrincipal() {
        System.out.println("\n Tria una opció:");
        System.out.println("0.-Sortir");
        System.out.println("1.-Por el archivo\n"
                + "2.-Por el teclado\n");
        System.out.print("nOpció: ");
    }
    private static final String TOTAL_DE = "Total de";

    private void ShowSecuences(String dnaSequence, String dnaSubSequence) {
        System.out.println("DNA sequence: " + dnaSequence);
        System.out.println("DNA subSequence: " + dnaSubSequence);
    }
}
