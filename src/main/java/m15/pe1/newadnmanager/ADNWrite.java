/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pe1.newadnmanager;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author jimmy
 */
public class ADNWrite {
/**
 * Recogemos y guardamos el valor escrito por teclado
 * @return secuencia
 */
    public String secuencia() {
        Scanner myScan = new Scanner(System.in);
        System.out.println();
        System.out.print("Escriba la cadena (ACGT): ");
        String secuencia = myScan.next();
        secuencia = secuencia.toUpperCase();
        return secuencia;
    }
}
